(function () {
    let editor;

    function validateChanges() {
        let saved = JSON.stringify($.jStorage.get('tokens'), null, 2);
        let val = editor.getText();
        document.getElementById('unsaved-changes').classList.toggle('hidden', saved == val);
    }

    function populateLegend() {
        let tbody = document.querySelectorAll('#legend tbody')[0];
        let legends = [
            '{c}{w}{u}{b}{r}{g}', '{t}', '{q}', '{e}', '{1}-{10}{0}{X}', 
            '{w/u}{u/b}{b/r}{r/g}', '{wp}{up}{bp}{rp}{gp}', '{2/w}{2/u}{2/b}{2/r}{2/g}'
        ];

        legends.forEach(value => {
            let row = document.createElement('tr');
            let key = document.createElement('td');
            key.classList = "key";
            let result = document.createElement('td');
            result.classList = "result";

            key.innerText = value;
            result.innerText = value;

            row.appendChild(key);
            row.appendChild(result);
            tbody.appendChild(row);
        })
    }

    function getDefaultData() {
        let treasureToken = {
            name: 'Treasure',
            count: 1,
            type: 'Artifact',
            ci: '{c}',
            text: '{t}, Sacrifice this artifact: Add one mana of any color.'
        };
        let spiritToken = {
            name: 'Spirit',
            count: 1,
            type: 'Creature',
            subtype: 'Spirit',
            ci: '{w}{b}',
            text: 'Flying',
            pt: '1/1'
        };

        let data = [spiritToken, treasureToken];
        return data;
    }

    function initialize() {
        populateLegend();

        // create the editor
        let container = document.getElementById('jsoneditor');
        let options = {
            mode: 'code',
            modes: ['code', 'form', 'text', 'tree', 'view'],
            onChangeText: validateChanges
        };
        editor = new JSONEditor(container, options);

        // set json
        let json = $.jStorage.get('tokens') || getDefaultData();
        editor.set(json);

        document.getElementById('saveDocument').onclick = function () {
            $.jStorage.set('tokens', editor.get());
            validateChanges();
        }

        validateChanges();
    }

    initialize();
})();