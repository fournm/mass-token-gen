(function() {

    function initialize() {
        let tokens = $.jStorage.get('tokens', []);
        let cards = document.querySelector('#cards');
        let card = document.querySelector('#card');
        tokens.forEach((token) => {
            // do some manipulation
            token.name = token.name || token.subtype;

            let clone = document.importNode(card.content, true);

            let header = clone.querySelector('header');
            header.textContent = token.name;

            let color = clone.querySelector('.color');
            color.textContent = token.ci;

            let type = clone.querySelector('.supertype');
            type.textContent = token.type;

            let subtype = clone.querySelector('.type');
            subtype.textContent = token.subtype;

            let text = clone.querySelector('.text');
            text.textContent = token.text;

            let footer = clone.querySelector('footer');

            if(token.skip) {
                return;
            }

            if(token.ptRanges) {
                let pts = token.ptRanges;
                for(let i = 0; i < pts.length; i++) {
                    footer.textContent = token.ptRanges[i].pt;
                    
                    for(let j = 0; j < parseInt(token.ptRanges[i].count, 10); j++) {
                        let entry = clone.cloneNode(true);
                        cards.appendChild(entry);
                    }
                }
            } else {
                footer.textContent = token.pt;

                for(let i = 0; i < parseInt(token.count, 10); i++) {
                    let entry = clone.cloneNode(true);
                    cards.appendChild(entry);
                }
                    
            }  
        });
    }

    initialize();
})();
